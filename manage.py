import os
from flask.cli import AppGroup
from flask_migrate import upgrade
from app.backend import create_app
from app.extensions import db as database


app = create_app(os.getenv('FLASK_ENV') or 'default')
database_cli = AppGroup('database')


@database_cli.command()
def rebuild():
    database.drop_all()
    database.engine.execute('DROP TABLE IF EXISTS alembic_version;')
    upgrade()


app.cli.add_command(database_cli)
