import os
from pathlib import Path


BASEDIR = Path('.')


class Config:
    APP_NAME = os.environ['APP_NAME']
    HOSTNAME = os.environ['HOSTNAME']
    VERSION = os.environ['VERSION']
    SECRET_KEY = os.environ['SECRET_KEY']

    DB_SERVICE = os.environ['DB_SERVICE']
    DB_USER = os.environ['DB_USER']
    DB_PASSWORD = os.environ['DB_PASSWORD']
    DB_HOST = os.environ['DB_HOST']
    DB_PORT = os.environ['DB_PORT']
    DB_NAME = os.environ['DB_NAME']
    DB_URI = f'{DB_SERVICE}://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}'

    TIMEZONE = os.environ['TIMEZONE']

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ENGINE_OPTIONS = {
        'pool_size': 150, 
        'pool_timeout': 0,
        'pool_recycle': 20,
        'max_overflow': 100,
    }

    BUNDLE_ERRORS = False

    JWT_EXPIRATION_SECONDS = 3600


class DevelopmentConfig(Config):
    FLASK_DEBUG = True
    TESTING = True

    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DB') or Config.DB_URI

    if SQLALCHEMY_DATABASE_URI.startswith('sqlite'):
        del(Config.SQLALCHEMY_ENGINE_OPTIONS['pool_size'])
        del(Config.SQLALCHEMY_ENGINE_OPTIONS['pool_timeout'])
        del(Config.SQLALCHEMY_ENGINE_OPTIONS['max_overflow'])


class TestingConfig(Config):
    FLASK_DEBUG = False
    TESTING = True

    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DB') or Config.DB_URI

    if SQLALCHEMY_DATABASE_URI.startswith('sqlite'):
        del(Config.SQLALCHEMY_ENGINE_OPTIONS['pool_size'])
        del(Config.SQLALCHEMY_ENGINE_OPTIONS['pool_timeout'])
        del(Config.SQLALCHEMY_ENGINE_OPTIONS['max_overflow'])


class ProductionConfig(Config):
    FLASK_DEBUG = False
    TESTING = False

    SQLALCHEMY_DATABASE_URI = os.environ.get('PROD_DB') or Config.DB_URI


config = {
    'development': DevelopmentConfig,
    'testing'    : TestingConfig,
    'production' : ProductionConfig,
    'default'    : DevelopmentConfig
}
