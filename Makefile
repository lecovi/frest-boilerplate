help:
	@echo "help                                   -- print this help"
	@echo ""
	@echo "build [stack=prod|dev]                 -- build docker stack (default: dev)"
	@echo "covtests                               -- run tests with coverage. HTML output available on htmlcov/"
	@echo "dbreuild                               -- drops DB and apply migrations"
	@echo "deploy                                 -- pull & up new stack for deployment"
	@echo "dockershell                            -- run bash inside docker"
	@echo "docs                                   -- builds Sphinx documentation"
	@echo "down [stack=prod|dev]                  -- clean artifacts (default: dev)"
	@echo "flaskshell                             -- run python Flask shell inside docker"
	@echo "migrate                                -- apply migrations (Flask-Migrate upgrade)"
	@echo "migrations MESSAGE=\"migration message\" -- make migrations (Flask-Migrate migrate)"
	@echo "ps [stack=prod|dev]                    -- show stack status (default: dev)"
	@echo "reload                                 -- restarts api container, use this if you change ENVIRONMENT"
	@echo "runserver                              -- runserver for development at 0.0.0.0"
	@echo "stop [stack=prod|dev]                  -- stop docker stack (default: dev)"
	@echo "tests                                  -- run tests using docker"
	@echo "up [stack=prod|dev]                    -- start docker stack (default: dev)"

RUN=docker-compose exec api
MANAGE=${RUN} pipenv run flask
COMPOSE_PROD=docker-compose -f docker-compose.prod.yml
RUN_PROD=${COMPOSE_PROD} exec api
COMPOSE_TEST=docker-compose -f docker-compose.test.yml
RUN_TEST=${COMPOSE_TEST} exec test-api

# If the first argument is "down"...
ifeq (down, $(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

# If the first argument is "build"...
ifeq (build, $(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "build"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

# If the first argument is "tests"...
ifeq (tests, $(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "build"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

build:
ifeq ($(stack), prod)
	@echo -e "\033[1;32m=========================\033[0m"
	@echo -e "\033[1;32mBuilding Production Stack\033[0m"
	@echo -e "\033[1;32m=========================\033[0m\n"
	@echo ""
	docker-compose build --no-cache $(RUN_ARGS)
else
	@echo -e "\033[1;36m=================\033[0m"
	@echo -e "\033[1;36mDevelopment Stack\033[0m"
	@echo -e "\033[1;36m=================\033[0m\n"
	docker-compose build $(RUN_ARGS)
endif

covtests:
	${COMPOSE_TEST} up -d
	${RUN_TEST} pipenv run py.test --cov=app/
	${RUN_TEST} pipenv run coverage html
	${COMPOSE_TEST} down -v

dbrebuild:
	${MANAGE} database rebuild

deploy:
	${COMPOSE_PROD} pull
	${COMPOSE_PROD} up -d

dockershell:
	${RUN} /bin/bash

docs:
	${RUN} pipenv run make html -C app/docs/

down:
ifeq ($(stack), prod)
	@echo -e "\033[1;32m================\033[0m"
	@echo -e "\033[1;32mProduction Stack\033[0m"
	@echo -e "\033[1;32m================\033[0m\n"
	${COMPOSE_PROD} down $(RUN_ARGS)
else
	@echo -e "\033[1;36m=================\033[0m"
	@echo -e "\033[1;36mDevelopment Stack\033[0m"
	@echo -e "\033[1;36m=================\033[0m\n"
	docker-compose down $(RUN_ARGS)
endif

flaskshell:
	${RUN} pipenv run flask shell

migrate:
	${MANAGE} db upgrade

migrations:
	${MANAGE} db migrate -m "$(MESSAGE)"

ps:
ifeq ($(stack), prod)
	@echo -e "\033[1;32m================\033[0m"
	@echo -e "\033[1;32mProduction Stack\033[0m"
	@echo -e "\033[1;32m================\033[0m\n"
	${COMPOSE_PROD} ps
else
	@echo -e "\033[1;36m=================\033[0m"
	@echo -e "\033[1;36mDevelopment Stack\033[0m"
	@echo -e "\033[1;36m=================\033[0m\n"
	docker-compose ps
endif

reload:
ifeq ($(stack), prod)
	@echo -e "\033[1;32m======================\033[0m"
	@echo -e "\033[1;32mUsing Production Stack\033[0m"
	@echo -e "\033[1;32m======================\033[0m\n"
	${COMPOSE_PROD} stop api
	${COMPOSE_PROD} start api
else
	@echo -e "\033[1;36m=======================\033[0m"
	@echo -e "\033[1;36mUsing Development Stack\033[0m"
	@echo -e "\033[1;36m=======================\033[0m\n"
	docker-compose stop api
	docker-compose start api
	${MANAGE} run --host=0.0.0.0
endif

	docker-compose stop api
	docker-compose start api

runserver:
	${MANAGE} run --host=0.0.0.0

tests:
	${COMPOSE_TEST} up -d
	${RUN_TEST} pipenv run py.test $(RUN_ARGS)
	${COMPOSE_TEST} down -v

stop:
ifeq ($(stack), prod)
	@echo -e "\033[1;32m=========================\033[0m"
	@echo -e "\033[1;32mStopping Production Stack\033[0m"
	@echo -e "\033[1;32m=========================\033[0m\n"
	${COMPOSE_PROD} stop
else
	@echo -e "\033[1;36m==========================\033[0m"
	@echo -e "\033[1;36mStopping Development Stack\033[0m"
	@echo -e "\033[1;36m==========================\033[0m\n"
	docker-compose stop
endif

up:
ifeq ($(stack), prod)
	@echo -e "\033[1;32m======================\033[0m"
	@echo -e "\033[1;32mUsing Production Stack\033[0m"
	@echo -e "\033[1;32m======================\033[0m\n"
	${COMPOSE_PROD} up -d
	${RUN_PROD} db upgrade
else
	@echo -e "\033[1;36m=======================\033[0m"
	@echo -e "\033[1;36mUsing Development Stack\033[0m"
	@echo -e "\033[1;36m=======================\033[0m\n"
	docker-compose up -d
	${MANAGE} db upgrade
	${MANAGE} run --host=0.0.0.0
endif

.PHONY: build dbrebuild deploy dockershell docs down flaskshell migrate migrations ps reload runserver tests stop up
