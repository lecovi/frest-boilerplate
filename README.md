# FREST Boilerplate

A simple flask boilerplate for REST APIs.

FREST stands for: Flask ReST API Boilerplate

## API

_TBD_

## Dev
### Install

1. Clone repo
2. Create `.env` from `.env.dist`
3. Build docker images with `make build`
4. Run with `make up`

```
cp .env.dist .env
make build
make up
```

### Usage

There is a Makefile that wraps most used commands. Use `make --help` to view details.

You can access app on http://localhost:8000 
Also, you have available a PgAdmin access to DB on http://localhost:8001.

### Install new packages

```bash
docker-compose exec web pipenv install new_package_name [--dev]
```

## Production

```
make up stack=prod
```

### Update

```
Update .env with current VERSION
make build stack=prod
make down stack=prod
make up stack=prod
```