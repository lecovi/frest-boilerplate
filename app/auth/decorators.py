from functools import wraps

from flask import g

from app.helpers import get_auth_token_from_header, AuthError
from .models import BlacklistToken, User


def login_required(f):
    """Determines if the access token is valid
    """
    @wraps(f)
    def decorated(*args, **kwargs):
        token = get_auth_token_from_header()
        
        blacklisted_token = BlacklistToken.get_by(token=token)
        if blacklisted_token:
            response = {
                'status': 'error',
                'message': 'Invalid Token',
                }
            raise AuthError(error=response, status_code=401)

        user_id = User.decode_auth_token(token)
        user = User.get_by(id=user_id)
        g.current_user = user
        g.auth_token = token
        
        return f(*args, **kwargs)
    return decorated
