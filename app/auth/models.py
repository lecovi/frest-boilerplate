from datetime import datetime, timedelta

from flask import current_app
from flask_bcrypt import generate_password_hash, check_password_hash
import jwt

from app.helpers import AuthError
from app.extensions import db, Base


class User(Base):
    """ User Model for storing user related details """
    __tablename__ = 'users'

    email = db.Column(db.String(128), nullable=False, unique=True, index=True)
    username = db.Column(db.String(128), nullable=False, unique=True, index=True)
    is_admin = db.Column(db.Boolean, default=False)
    password_hash = db.Column(db.String(256))

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password).decode()

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    @property
    def is_active(self):
        return self.erased == False
    
    def encode_auth_token(self):
        """
        Generates the Auth Token

        Payload:
                - exp: expiration date of the token
                - iat: the time the token is generated
                - sub: the subject of the token (the user whom it identifies)
        :return: string
        """
        payload = {
            'exp': datetime.utcnow() + timedelta(seconds=current_app.config['JWT_EXPIRATION_SECONDS']),
            'iat': datetime.utcnow(),
            'sub': self.id
        }
        try:
            token = jwt.encode(payload, current_app.config.get('SECRET_KEY'), algorithm='HS256')
            return token
        except Exception as e:
            return e
    
    @staticmethod
    def decode_auth_token(auth_token):
        """
        Decodes the auth token
        :param auth_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(auth_token, current_app.config.get('SECRET_KEY'))
            return payload['sub']
        except jwt.ExpiredSignatureError:
            error = {
                'status': 'ExpiredSignatureError',
                'message': 'Signature expired. Please log in again.',
            }
            raise AuthError(error, status_code=404)
        except jwt.InvalidTokenError:
            error = {
                'status': 'InvalidTokenError',
                'message': 'Invalid token. Please log in again.',
            }
            raise AuthError(error, status_code=404)


class BlacklistToken(Base):
    """
    Token Model for storing JWT tokens
    """
    __tablename__ = 'blacklist_tokens'

    token = db.Column(db.String(500), unique=True, nullable=False)
