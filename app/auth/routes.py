from flask import Blueprint, request, jsonify, g
from flask.views import MethodView
from flask_restful import Api, Resource, fields, reqparse, marshal_with, abort

from app.helpers import get_auth_token_from_header
from .models import User, BlacklistToken
from .decorators import login_required


auth_blueprint = Blueprint('auth', __name__, url_prefix='/v1/auth')
api = Api(auth_blueprint)

user_fields = {
    'id': fields.Integer,
    'username': fields.String,
    'email': fields.String,
    'created_on': fields.DateTime(),
    'updated_on': fields.DateTime(),
}


class RegisterResource(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('password', type=str, required=True, location='json')
        self.reqparse.add_argument('username', type=str, location='json')
        self.reqparse.add_argument('email', type=str, location='json')

    @marshal_with(user_fields)
    def post(self):
        args = self.reqparse.parse_args()

        if args['username'] is None or args['email'] is None:
            return abort(400, message='Missing parameters!')

        # check if email is in DB
        user = User.get_by(email=args['email'])
        if user:
            return abort(400, message='email already exists, please login.')
        
        # check if username is in DB
        user = User.get_by(username=args['username'])
        if user:
            return abort(400, message='username already exists, please login.')

        user = User(username=args['username'], password=args['password'], email=args['email'])
        user.save()
        return user, 201


class LoginResource(Resource):
    """ User Login API Resource 
    
        :param username: User's email or username.
        :param password: User's password.
        :rtype auth_token: A str with the authentication token. 
    """
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('password', type=str, required=True, location='json')
        self.reqparse.add_argument('username', type=str, required=True, location='json')

    def post(self):
        args = self.reqparse.parse_args()

        user = User.get_by(email=args['username'])
        if user is None:
            user = User.get_by(username=args['username'])
            if user is None:
                return abort(404, message='username/password not valid.')

        if user.verify_password(password=args['password']):
            auth_token = user.encode_auth_token().decode()
        else:
            return abort(404, message='username/password not valid.')

        return {'auth_token': auth_token}


class LogoutResource(Resource):
    """ User Logout API Resource 
    
        :param auth_token: User's auth_token in request header.
        :rtype message: Error/Success message.
    """
    def post(self):
        token = get_auth_token_from_header()

        blacklisted_token = BlacklistToken.get_by(token=token)
        if blacklisted_token:
            return abort(404, message='Invalid Token.')

        # This should never happen
        user_id = User.decode_auth_token(token)
        user = User.get_by(id=user_id)
        if user is None:
            return abort(404, message='Invalid Token.')

        blacklisted_token = BlacklistToken(token=token)
        blacklisted_token.save()

        response = {
            'status': 'success',
            'message': 'Successfully logged out.',
        }
        return response


class UserProfileResource(Resource):
    """
    User Resource
    """
    @login_required
    @marshal_with(user_fields)
    def get(self):
        return g.current_user


class ChangePasswordResource(Resource):
    """ Change password API Resource

        Endpoint will receive new password, verifies it and change it. Then the current
        token will be blacklisted and a new one will be generated.
    
        :param password: User's current password.
        :param new_password: User's new password.
        :param confirm_new_password: User's new password.
        :rtype auth_token: A str with the authentication token. 
    """
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('password', type=str, required=True, location='json')
        self.reqparse.add_argument('new_password', type=str, required=True, location='json')
        self.reqparse.add_argument('confirm_new_password', type=str, required=True, location='json')

    @login_required
    def post(self):
        args = self.reqparse.parse_args()

        if not g.current_user.verify_password(password=args['password']):
            return abort(401, message='Password not correct!')

        if args['new_password'] != args['confirm_new_password']:
            return abort(400, message="Password doesn't match!")

        g.current_user.password = args['new_password']
        g.current_user.save()

        auth_token = g.current_user.encode_auth_token().decode()

        blacklisted_token = BlacklistToken(token=g.auth_token)
        blacklisted_token.save()

        return {'auth_token': auth_token}


api.add_resource(RegisterResource, '/register')
api.add_resource(LoginResource, '/login')
api.add_resource(LogoutResource, '/logout')
api.add_resource(UserProfileResource, '/profile')
api.add_resource(ChangePasswordResource, '/change_password')
