from datetime import datetime, timedelta, date, time

import pytz
import tzlocal
from flask import abort, current_app
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from config import Config


db = SQLAlchemy()
migrate = Migrate()
bcrypt = Bcrypt()


def _get_now_with_timezone():
    # return pytz.utc.localize(datetime.utcnow()).astimezone(tzlocal.get_localzone()).now()
    return datetime.now(pytz.timezone(Config.TIMEZONE))


def _get_to_date_for_between():
    to_date = datetime.now() + timedelta(days=1)
    return to_date.strftime(current_app.config['URL_DATETIME_FORMAT'])


class Base(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True, index=True)
    created_on = db.Column(db.DateTime(timezone=True),
                           default=lambda: _get_now_with_timezone(),
                           nullable=False)
    updated_on = db.Column(db.DateTime(timezone=True),
                           default=lambda: _get_now_with_timezone(),
                           nullable=False)
    erased = db.Column(db.Boolean, nullable=False, default=False)

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.id}>'

    @classmethod
    def all(cls, include_erased=False):
        return db.session.query(cls).filter_by(erased=include_erased).all()

    @classmethod
    def get_by(cls, erased=False, **kwargs):
        return db.session.query(cls).filter_by(erased=erased, **kwargs).first()

    @classmethod
    def get_or_404(cls, pk, erased=False):
        """Get ScriptItem using id or returns 404.
        """
        item = cls.get_by(id=pk, erased=erased)
        if item is None:
            abort(404)
        return item

    @classmethod
    def get_between_dates(cls, from_date, to_date=None):
        if to_date is None:
            to_date = _get_to_date_for_between()
        return cls.query.filter(cls.updated_on.between(from_date, to_date)).all()

    def update(self):
        raise NotImplementedError

    def set_erased(self):
        self.erased = True
        self.save()

    def restore(self):
        self.erased = False
        self.save()

    def save(self, commit=True):
        db.session.add(self)
        if commit == True:
            db.session.commit()
            current_app.logger.debug(f'{self} saved to DB')

    def to_dict(self, exclude=None):
        """
            This will return a dict instance of model. It works with jsonify.
        :param exclude: attribute name to exclude in response.
        :return: a dict().
        """
        response = dict()
        for attr, value in self.__dict__.items():
            if attr.startswith('_'):
                continue
            if exclude and attr in exclude:
                continue
            if isinstance(value, (date, time, datetime)):
                timezone = pytz.timezone(Config.TIMEZONE)
                value = value.astimezone(timezone).isoformat()
            if isinstance(value, timedelta):
                value = value.total_seconds()
            response.update({attr: value})
        return response



@db.event.listens_for(Base, 'before_update', propagate=True)
def timestamp_before_update(mapper, connection, target):
    """Database Trigger to update field updated_on"""
    target.updated_on = datetime.now(pytz.timezone(Config.TIMEZONE))
