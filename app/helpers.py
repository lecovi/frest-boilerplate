from flask import request, jsonify


class AuthError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code


def handle_auth_error(ex):
    response = jsonify(ex.error)
    response.status_code = ex.status_code
    return response


def get_auth_token_from_header():
    auth_header = request.headers.get('Authorization')

    if auth_header is None:
        response = {
            'status': 'error',
            'message': 'Authorization header is expected',
        }
        raise AuthError(error=response, status_code=400)

    try:
        bearer, token = auth_header.split(' ')
    except ValueError:
        response = {
            'status': 'error',
            'message': 'Authorization header is malformed',
        }
        raise AuthError(error=response, status_code=400)

    if bearer.lower() != 'bearer':
        response = {
            'status': 'error',
            'message': 'Authorization header must start with "Bearer"',
        }
        raise AuthError(error=response, status_code=400)

    return token