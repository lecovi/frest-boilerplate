from flask import url_for


def test_exite_app(app):
    assert not app is None


def test_app_es_testing(app):
    assert app.config['TESTING']


def test_index(app, client):
    response = client.get(url_for('main.index'))
    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'
    assert app.config['APP_NAME'] == response.json['config']['APP_NAME']
    assert app.config['VERSION'] == response.json['config']['VERSION']


def test_version(app, client):
    response = client.get(url_for('main.version'))
    assert response.status_code == 200
    assert 'text/html' in response.headers['Content-Type']
    assert app.config['APP_NAME'] in str(response.data)
    assert app.config['VERSION'] in str(response.data)


def test_404(app, client):
    response = client.get('/non-existent-url')
    assert response.status_code == 404