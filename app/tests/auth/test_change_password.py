from app.auth.models import BlacklistToken
from app.auth.routes import api, ChangePasswordResource


def test_change_password(client, users):
    new_password = 'bar'
    data = {
        'password': users[0].username,
        'new_password': new_password,
        'confirm_new_password': new_password,
    }
    auth_token = users[0].encode_auth_token().decode()
    headers = [
        ('Authorization', f'Bearer {auth_token}')
    ]
    response = client.post(api.url_for(ChangePasswordResource), headers=headers, json=data)
    blacklisted_token = BlacklistToken.get_by(token=auth_token)
    assert response.status_code == 200
    assert 'auth_token' in response.json.keys()
    assert users[0].verify_password(password=new_password)
    assert blacklisted_token.token == auth_token


def test_change_password_error_wrong_password(client, users):
    new_password = 'bar'
    data = {
        'password': users[1].username,
        'new_password': new_password,
        'confirm_new_password': new_password,
    }
    auth_token = users[0].encode_auth_token().decode()
    headers = [
        ('Authorization', f'Bearer {auth_token}')
    ]
    response = client.post(api.url_for(ChangePasswordResource), headers=headers, json=data)
    assert response.status_code == 401
    assert 'message' in response.json.keys()


def test_change_password_error_new_password_mismatch(client, users):
    new_password = 'bar'
    data = {
        'password': users[0].username,
        'new_password': new_password,
        'confirm_new_password': f'{new_password}1',
    }
    auth_token = users[0].encode_auth_token().decode()
    headers = [
        ('Authorization', f'Bearer {auth_token}')
    ]
    response = client.post(api.url_for(ChangePasswordResource), headers=headers, json=data)
    assert response.status_code == 400
    assert 'message' in response.json.keys()
