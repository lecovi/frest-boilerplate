from app.auth.routes import api, LogoutResource
from app.auth.models import BlacklistToken


def test_logout(client, users):
    auth_token = users[0].encode_auth_token().decode()
    headers = [
        ('Authorization', f'Bearer {auth_token}')
    ]
    response = client.post(api.url_for(LogoutResource), headers=headers)
    assert response.status_code == 200
    assert 'message' in response.json.keys()
    assert response.json['status'] == 'success'


def test_logout_error_blacklisted_token(client, users):
    auth_token = users[0].encode_auth_token().decode()
    blacklisted_token = BlacklistToken(token=auth_token)
    blacklisted_token.save()
    headers = [
        ('Authorization', f'Bearer {auth_token}')
    ]
    response = client.post(api.url_for(LogoutResource), headers=headers)
    assert response.status_code == 404
    assert 'message' in response.json.keys()
