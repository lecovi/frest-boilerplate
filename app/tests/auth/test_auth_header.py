from app.auth.routes import api, LogoutResource
from app.auth.models import BlacklistToken


def test_auth_header_error_with_no_header(client, users):
    headers = [
    ]
    response = client.post(api.url_for(LogoutResource), headers=headers)
    assert response.status_code == 400
    assert 'message' in response.json.keys()


def test_auth_header_error_with_malformed_header(client, users):
    auth_token = users[0].encode_auth_token().decode()
    headers = [
        ('Authorization', f'Bearer')
    ]
    response = client.post(api.url_for(LogoutResource), headers=headers)
    assert response.status_code == 400
    assert 'message' in response.json.keys()

def test_auth_header_error_without_bearer(client, users):
    auth_token = users[0].encode_auth_token().decode()
    headers = [
        ('Authorization', f'bearrrer {auth_token}')
    ]
    response = client.post(api.url_for(LogoutResource), headers=headers)
    assert response.status_code == 400
    assert 'message' in response.json.keys()
