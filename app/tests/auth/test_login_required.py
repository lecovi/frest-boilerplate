from app.auth.models import BlacklistToken
from app.auth.routes import api, UserProfileResource


def test_login_required_error(client, users):
    auth_token = users[0].encode_auth_token().decode()
    blacklisted_token = BlacklistToken(token=auth_token)
    blacklisted_token.save()
    headers = [
        ('Authorization', f'Bearer {auth_token}')
    ]
    response = client.get(api.url_for(UserProfileResource), headers=headers)
    assert response.status_code == 401
    assert response.json['status'] == 'error'
    assert 'message' in response.json.keys()
