from email.utils import formatdate
from calendar import timegm
from app.auth.routes import api, UserProfileResource


def test_user_profile(client, users):
    auth_token = users[0].encode_auth_token().decode()
    headers = [
        ('Authorization', f'Bearer {auth_token}')
    ]
    response = client.get(api.url_for(UserProfileResource), headers=headers)
    assert response.status_code == 200
    assert users[0].id == response.json['id']
    assert users[0].email == response.json['email']
    assert users[0].username == response.json['username']
    assert formatdate(timegm(users[0].created_on.utctimetuple())) == response.json['created_on']
    assert formatdate(timegm(users[0].updated_on.utctimetuple())) == response.json['updated_on']
    assert 'password' not in response.json.keys()
