import pytest
from app.auth.models import User


def test_not_readable_password():
    with pytest.raises(AttributeError):
        user = User(username='foo', email='foo@mail.com', password='foo')
        user.password


def test_user_is_active(app, users):
    assert users[0].is_active


def test_user_is_not_active(app, users):
    users[0].set_erased()
    assert not users[0].is_active


def test_user_erase_and_restore(app, users):
    users[0].set_erased()
    assert not users[0].is_active
    users[0].restore()
    assert users[0].is_active
