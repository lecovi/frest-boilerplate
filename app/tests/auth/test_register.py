from app.auth.routes import api, RegisterResource
from app.auth.models import User


def test_register_with_username(client):
    data = {
        'username': 'foo',
        'password': 'foo',
        'email': 'foo@mail.com',
    }
    response = client.post(api.url_for(RegisterResource), json=data)
    assert response.status_code == 201
    user = User.get_by(username=data['username'])
    assert user.email == data['email']
    assert user.verify_password(password=data['password'])


def test_register_error_without_username(client):
    data = {
        'password': 'foo',
        'email': 'foo@mail.com',
    }
    response = client.post(api.url_for(RegisterResource), json=data)
    assert response.status_code == 400
    assert 'message' in response.json.keys()


def test_register_error_without_email(client):
    data = {
        'username': 'foo',
        'password': 'foo',
    }
    response = client.post(api.url_for(RegisterResource), json=data)
    assert response.status_code == 400
    assert 'message' in response.json.keys()


def test_register_error_without_password(client):
    data = {
        'username': 'foo',
        'email': 'foo@mail.com',
    }
    response = client.post(api.url_for(RegisterResource), json=data)
    assert response.status_code == 400
    assert 'message' in response.json.keys()


def test_register_error_with_existent_username(client, users):
    data = {
        'username': users[0].username,
        'password': 'password',
        'email': 'email',
    }
    response = client.post(api.url_for(RegisterResource), json=data)
    assert response.status_code == 400
    assert 'message' in response.json.keys()


def test_register_error_with_existent_email(client, users):
    data = {
        'username': 'username',
        'password': 'password',
        'email': users[0].email,
    }
    response = client.post(api.url_for(RegisterResource), json=data)
    assert response.status_code == 400
    assert 'message' in response.json.keys()
