from app.auth.routes import api, LoginResource


def test_login_with_username(client, users):
    data = {
        'username': users[0].username,
        'password': users[0].username,
    }
    response = client.post(api.url_for(LoginResource), json=data)
    assert response.status_code == 200
    assert 'auth_token' in response.json.keys()


def test_login_with_email(client, users):
    data = {
        'username': users[0].email,
        'password': users[0].username,
    }
    response = client.post(api.url_for(LoginResource), json=data)
    assert response.status_code == 200
    assert 'auth_token' in response.json.keys()


def test_login_error_without_username(client, users):
    data = {
        'password': users[0].username,
    }
    response = client.post(api.url_for(LoginResource), json=data)
    assert response.status_code == 400
    assert 'message' in response.json.keys()


def test_login_error_without_password(client, users):
    data = {
        'username': users[0].username,
    }
    response = client.post(api.url_for(LoginResource), json=data)
    assert response.status_code == 400
    assert 'message' in response.json.keys()


def test_login_error_without_username_and_password(client, users):
    data = {
    }
    response = client.post(api.url_for(LoginResource), json=data)
    assert response.status_code == 400
    assert 'message' in response.json.keys()


def test_login_error_with_wrong_password(client, users):
    data = {
        'username': users[0].email,
        'password': users[1].username,
    }
    response = client.post(api.url_for(LoginResource), json=data)
    assert response.status_code == 404
    assert 'message' in response.json.keys()


def test_login_error_with_wrong_username(client, users):
    data = {
        'username': 'no user',
        'password': users[1].username,
    }
    response = client.post(api.url_for(LoginResource), json=data)
    assert response.status_code == 404
    assert 'message' in response.json.keys()
