import pytest
from flask_migrate import upgrade

from app.extensions import db
from app.backend import create_app
from app.auth.models import User


@pytest.fixture
def app():
    app = create_app('testing')
    with app.app_context():
        db.create_all()
        upgrade()
        yield app
        db.drop_all()


@pytest.fixture
def users():
    users = []
    usernames = ['foo', 'bar', 'baz']
    for username in usernames:
        user = User(username=username, password=username, email=f'{username}@mail.com')
        user.save()
        users.append(user)
    return users
