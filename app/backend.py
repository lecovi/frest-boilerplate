# Standard lib imports
# Third-party imports
from flask import Flask, jsonify, Blueprint, current_app, render_template, url_for
# BITSON imports
from config import config
from app.extensions import db, migrate, CORS, bcrypt
from app.helpers import AuthError, handle_auth_error
from app.auth.routes import auth_blueprint


main = Blueprint('main', __name__)

BLUEPRINTS = [
    # Import your blueprints and add it here
    main,
    auth_blueprint,
]

def error404(e):
    return render_template('404.html'), 404


def error500(e):
    return render_template('500.html'), 500


def create_app(config_name):
    app = Flask(__name__)
    CORS(app)
    app.config.from_object(config[config_name])
    app.logger.debug(f"SQLALCHEMY_DATABASE_URI={app.config['SQLALCHEMY_DATABASE_URI']}")

    bcrypt.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)

    for blueprint in BLUEPRINTS:
        app.register_blueprint(blueprint)

    app.register_error_handler(404, error404)
    app.register_error_handler(500, error500)
    app.register_error_handler(AuthError, handle_auth_error)

    return app


@main.route('/')
def index():
    response = dict(
        version=current_app.config['VERSION']
    )
    if current_app.config['DEBUG']:
        from datetime import timedelta
        response['config'] = {
            k:v.total_seconds() if isinstance(v, timedelta) else v 
                for k, v in current_app.config.items()
                }

    return jsonify(response)


@main.route('/version')
def version():
    return render_template('version.html', name=current_app.config['APP_NAME'])
