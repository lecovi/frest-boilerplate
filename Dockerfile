FROM python:3.7-slim-stretch as debian

ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH /code:$PYTHONPATH

RUN apt-get update
RUN apt-get install -y libffi-dev libpango1.0-dev libcairo2-dev

FROM debian as pipenv

RUN pip install pipenv

FROM pipenv

RUN useradd lecovi --home-dir /home/lecovi --create-home --uid 1000 --user-group --shell /bin/bash
USER lecovi
RUN mkdir /home/lecovi/code

# Copy code
WORKDIR /home/lecovi/code/
COPY --chown=lecovi:lecovi . /home/lecovi/code/

# Install dependencies
RUN pipenv install --dev

EXPOSE 8000

CMD ["tail", "-f", "/dev/null"]
